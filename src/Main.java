import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4,5};
        System.out.println("main before " + Arrays.toString(arr));
        method_1(arr);
        System.out.println("main  after " + Arrays.toString(arr));
    }

    private static void method_1(int[] arr){
        System.out.println("method_1 before " + Arrays.toString(arr));
        arr = null;
        method_2(arr);
        System.out.println("method_1 after " + Arrays.toString(arr));
    }

    private static void method_2(int[] arr ){
        System.out.println("method_2 before " + Arrays.toString(arr));
        arr = new int[]{5,4,3,2,1};
        System.out.println("method_2 " + Arrays.toString(arr));
    }
}
