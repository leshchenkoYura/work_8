package work_8;

public class Work_8 {
    private static final String THREAD_NAME_1 = "thread 1";
    private static final String THREAD_NAME_2 = "thread 2";

    public static void main(String[] args) throws Throwable {
        switch (0) {
            case 0:
                RunnableExample example1 = new RunnableExample(THREAD_NAME_1, 1000);
                RunnableExample example2 = new RunnableExample(THREAD_NAME_2, 4000);
                Thread thread1 = new Thread(example1, THREAD_NAME_1);
                Thread thread2 = new Thread(example2, THREAD_NAME_2);
                thread1.start();
                thread2.start();
                System.out.println("start after");
                Thread.sleep(5000);
                System.out.println("thread name  " + thread1.getName());
                System.out.println("thread name  " + thread2.getName());
                example1.onStop();
                System.out.println("stop ".concat(THREAD_NAME_1));
                example2.onStop();
                System.out.println("stop ".concat(THREAD_NAME_2));
                System.out.println("dispose");
                break;
            case 1:
                for (int i = 0; i < 10; i++) {
                    new Thread("" + i) {
                        public void run() {
                            System.out.println("Thread: " + getName() + " running");
                        }
                    }.start();
                }
                break;
            case 2:
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Runnable running");
                    }
                }, "Example thread");
                thread.start();
                System.out.println(thread.getName());
                break;
            case 3:
                Runnable runnable =
                        () -> {
                            System.out.println("Lambda Runnable running");
                        };
                runnable.run();
                break;
            default:
                throw new Throwable("error select");
        }


    }

}
