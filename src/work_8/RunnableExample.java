package work_8;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class RunnableExample implements Runnable {
    private  AtomicInteger count;
    private  AtomicReference<String> name;
    private volatile boolean stop = true;
    private volatile int interval = 50;

    public RunnableExample(String name, int interval) {
        this.interval = interval;
        count = new AtomicInteger();
        this.name = new AtomicReference<>();
        this.name.set(name);
    }

    public void onStop() {
        this.stop = false;
    }

    @Override
    public void run() {
        while (keepRunning()) {
            count.set(count.get() + 1);
            System.out.println("Running name " + name.get() + " count ".concat(String.valueOf(count.get())));
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                count.set(0);
                System.out.println(e.getMessage());
            }
        }
    }

    private synchronized boolean keepRunning() {
        return this.stop;
    }


}
